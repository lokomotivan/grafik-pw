<?php namespace ProcessWire;

include('_functions.php');
$config->cart = json_decode(input()->cookie('cart'), 1) ?: [];

echo wireRenderFile('fields/_header');
echo wireRenderFile('fields/_breadcrumbs');
echo wireRenderFile('fields/_footer');
