<?php namespace ProcessWire;
// cart logic
$pid = sanitizer()->int(input('get')->pid);
$row = sanitizer()->int(input('get')->row);
$col = sanitizer()->int(input('get')->col);

// the $cart variable is set in _init.php
$cart = config('cart');

// function to add product to cart
function addtocart($cart, $pid, $row, $col) {
  // find the product
  $product = pages($pid);
  if(!$product->id) return;

  // we found a product, so add this product to the cart cookie
  $cart[] = [
    'pid' => $pid,
    'amount' => $product->pricing->getCell(0, $col),
    'size' => $product->pricing->getCell($row, 0),
    'price' => $product->pricing->getCell($row, $col),
  ];

  // set new cookie
  setcookie('cart', json_encode($cart), strtotime('+30 days'), '/');
}

// get cart and add product
if($pid > 0 OR $row > 0 OR $col > 0) {
  // do the logic
  addtocart($cart, $pid, $row, $col);
  // add this product to the cart
  session()->redirect($page->url);
}

// delete product
if($del = (int)input()->get('del')) {
  unset($cart[$del-1]);
  setcookie('cart', json_encode($cart), strtotime('+30 days'), '/');
  session()->redirect($page->url);
}

// calculate sum
$sum = 0;
foreach($cart as $item) if($item['price'] > 0) $sum += $item['price'];

// send email
$post = input('post');
if($post->mail) {
  // form was submitted
  echo wireRenderFile('fields/_cart_success.php', ['sum' => $sum]);
  return;
}
?>

<region id="mainsection">
<div class="uk-container">
<form id="order-form" action="./" method="post" class="uk-form-horizontal">

    <section class="tm-section">
        <div class="uk-container">
            <?= $page->render('title') ?>
            <?= $page->body ?>
        </div>
    </section>

    <section class="tm-section">
        <div class="content uk-padding-remove uk-overflow-auto">

                <!-- cart table -->
                <table id="cart-table" class="uk-table uk-table-striped uk-table-middle uk-margin-remove">
                    <caption></caption>
                    <thead>
                        <tr>
                            <th>Produkt</th>
                            <th class="uk-text-center" colspan="2">Richtpreis*</th>
                        </tr>
                    </thead>
                 <tbody>
                    <tr>
                        <td class="uk-width-1-1">
                           <strong class="uk-h4">Gratis Designvorschlag</strong><br>
                           Einer unserer Gesellen nimmt innerhalb von 24 Stunden mit Ihnen Kontakt auf.
                        </td>
                        <td class="price" colspan="2">0</td>

                    </tr>
                   <?php foreach($cart as $i=>$item): ?>
                       <?php
                            $product = pages($item['pid']);
                            if(!$product->id) continue;
                            ?>
                        <tr>
                            <td class="uk-table-expand">
                                <strong class="uk-h4"><a href="<?= $product->url ?>"><?= $product->title ?></a></strong><br>
                                <strong>Anzahl:</strong> <?= $item['amount'] ?>
                                <br>
                                <strong>Variante:</strong> <?= $item['size'] ?>
                                <?php if ($product->body):?>
                                    <hr class="uk-margin-small" />
                                    <p class="uk-margin-small uk-text-small"><?= truncateText($product->body) ?></p>
                                <?php endif;?>
                                <textarea class="uk-textarea uk-margin-small uk-width-1-1" name="itemcomments[]" placeholder="Ihr Kommentar zu '<?= $product->title ?>'" rows=2></textarea>
                            </td>
                            <td class="price uk-table-shrink"><?= $item['price'] ?: 0 ?></td>
                            <td class="actions uk-table-shrink">
                                <a href="./?del=<?= ($i+1) ?>"><span uk-icon="icon: trash"></span></a>
                            </td>
                        </tr>
                   <?php endforeach; ?>
                   <tr>
                       <td class="uk-h3 uk-margin-remove">Richtpreis Summe*</td>
                       <td class="total uk-text-center" colspan="2">
                           <span class="uk-h2"><?= $sum ?> €</span>
                       </td>
                   </tr>
                 </tbody>
               </table>

        </div>
    </section>

    <section class="tm-section">
        <div class="content uk-padding">
            <div class="uk-margin">
              <label class="uk-form-label" for="form-horizontal-text">Name</label>
              <div class="uk-form-controls">
                <input class="uk-input" type="text" name="name" placeholder="Max & Maria Muster">
              </div>
            </div>

            <div class="uk-margin">
              <label class="uk-form-label" for="form-horizontal-text">E-Mail</label>
              <div class="uk-form-controls">
                <input class="uk-input" type="text" name="mail" placeholder="max.maria@muster.at" required>
              </div>
            </div>

            <div class="uk-margin">
              <label class="uk-form-label" for="form-horizontal-text">Telefon</label>
              <div class="uk-form-controls">
                <input class="uk-input" type="text" name="tel" placeholder="0680 12 34 567">
              </div>
            </div>

            <div class="uk-margin">
              <label class="uk-form-label" for="form-horizontal-text">Kommentar</label>
              <div class="uk-form-controls">
                <textarea class="uk-textarea" name="comment" placeholder="Ihr Kommentar" rows=5></textarea>
              </div>
            </div>

            <div class="uk-margin uk-hidden">
              <label class="uk-form-label" for="form-horizontal-text">Bitte leer lassen!</label>
              <div class="uk-form-controls">
                <input class="uk-input" name="password" placeholder="Bitte dieses Feld leer lassen!" autocomplete="off">
              </div>
            </div>

            <div class="uk-text-center uk-margin-large-top">
                <button type="submit" class="uk-button uk-button-large uk-button-primary">
                    Unverbindliches Angebot anfragen
                </button>
            </div>
        </div>
    </section>

</form>
</div>
</region>

<div id="main">
  <?= $page->render('title') ?>

  <?= $page->body ?>

  <form action="./" method="post" class="uk-form-horizontal">

    <table class="uk-table uk-table-striped">
      <caption></caption>
      <thead>
        <tr>
          <th></th>
          <th>Produkt</th>
          <th>Richtpreis*</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td class="uk-width-1-1">
            <strong>Gratis Designvorschlag</strong><br>
            Einer unserer Gesellen nimmt innerhalb von 24 Stunden mit Ihnen Kontakt auf.
          </td>
          <td class="price">0</td>
        </tr>
        <?php foreach($cart as $i=>$item): ?>
          <?php
          $product = pages($item['pid']);
          if(!$product->id) continue;
          ?>
          <tr>
            <td><a href="./?del=<?= ($i+1) ?>"><span uk-icon="icon: trash"></span></a></td>
            <td class="uk-width-1-1">
              <strong><a href="<?= $product->url ?>"><?= $product->title ?></a></strong><br>
              <strong>Anzahl:</strong> <?= $item['amount'] ?><br>
              <strong>Variante:</strong> <?= $item['size'] ?><br>
              <p><?= truncateText($product->body) ?></p>

              <textarea class="uk-textarea uk-width-1-1" name="itemcomments[]" placeholder="Ihr Kommentar zu '<?= $product->title ?>'" rows=2></textarea>
            </td>
          <td class="price"><?= $item['price'] ?: 0 ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

    <div class="uk-padding-large">
      <h3 class="uk-text-center">Richtpreis Summe* <span class="uk-margin-left"><?= $sum ?> €</span></h3>
    </div>


    <div class="uk-margin">
      <label class="uk-form-label" for="form-horizontal-text">Name</label>
      <div class="uk-form-controls">
        <input class="uk-input" type="text" name="name" placeholder="Max & Maria Muster">
      </div>
    </div>

    <div class="uk-margin">
      <label class="uk-form-label" for="form-horizontal-text">E-Mail</label>
      <div class="uk-form-controls">
        <input class="uk-input" type="text" name="mail" placeholder="max.maria@muster.at" required>
      </div>
    </div>

    <div class="uk-margin">
      <label class="uk-form-label" for="form-horizontal-text">Telefon</label>
      <div class="uk-form-controls">
        <input class="uk-input" type="text" name="tel" placeholder="0680 12 34 567">
      </div>
    </div>

    <div class="uk-margin">
      <label class="uk-form-label" for="form-horizontal-text">Kommentar</label>
      <div class="uk-form-controls">
        <textarea class="uk-textarea" name="comment" placeholder="Ihr Kommentar" rows=5></textarea>
      </div>
    </div>

    <div class="uk-margin uk-hidden">
      <label class="uk-form-label" for="form-horizontal-text">Bitte leer lassen!</label>
      <div class="uk-form-controls">
        <input class="uk-input" name="password" placeholder="Bitte dieses Feld leer lassen!" autocomplete="off">
      </div>
    </div>

    <div class="uk-text-center uk-margin-large">
      <input type="submit" class="uk-button uk-button-large uk-button-primary" value="Unverbindliches Angebot anfragen" />
    </div>

  </form>

  <?= $page->shortbody ?>

</div>
