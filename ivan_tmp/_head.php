<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Graphic Desgin</title>
        <link rel="shortcut icon" href="docs/images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

        <!-- css -->
        <link rel="stylesheet" href="css/swiper.min.css">
        <link rel="stylesheet" href="css/theme.css">

        <!-- scripts -->
        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
        <script src="js/cart.js"></script>
        <script src="js/swiper.min.js"></script>
        <script src="js/js.cookie.js"></script>

        <link rel="stylesheet" href="css/custom.css">
        <script src="js/main.js"></script>

    </head>

    <body>

        <section id="header">
            <div class="uk-container">

                <div id="headerbar" class="uk-flex uk-flex-middle uk-flex-between">
                    <div class="logo">
                        <a href="index.php">
                            <img src="./images/logo.png" alt="logo" />
                        </a>
                    </div>
                    <div class="uk-flex">
                        <div class="social">
                            <a href="#">
                                <span class="svg-icon"><?php echo file_get_contents("images/facebook.svg");?></span>
                            </a>
                            <a href="#">
                                <span class="svg-icon"><?php echo file_get_contents("images/instagram.svg");?></span>
                            </a>
                        </div>
                        <?php

                            $productCount = 0;
                            if(isset($_SESSION["cart"])) {
                                $productCount = count($_SESSION["cart"]);
                            }

                        ?>
                        <button id="top-cart" href="#cart" uk-toggle value="<?=$productCount?>">
                            <span class="svg-icon"><?php echo file_get_contents("images/shopping-basket.svg");?></span>
                            <span id="top-cart-count" class="uk-badge"><?=$productCount?></span>
                        </button>
                    </div>
                </div>

                <div id="navbar">
                    <nav class="uk-navbar-container" uk-navbar>
                        <div class="uk-navbar-center uk-width-1-1">
                            <ul class="uk-navbar-nav uk-width-1-1">
                                <li class="uk-width-1-5 uk-active uk-parent">
                                    <a href="#">Event Wenburg</a>
                                    <div class="uk-navbar-dropdown uk-navbar-dropdown-width-5">
                                        <div class="uk-navbar-dropdown-grid" uk-grid>
                                            <div class="dropdown-img uk-width-medium">
                                                <div class="dropdown-img uk-cover-container">">
                                                    <img src="./images/navbar-img.jpg" uk-cover/>
                                                </div>
                                            </div>
                                            <div class="dropdown-items uk-width-expand">
                                                <ul class="uk-nav uk-navbar-dropdown-nav uk-column-1-3">
                                                    <?php for($i = 0; $i < 21; $i++):?>
                                                        <li><a href="product.php">Item <?=$i?></a></li>
                                                    <?php endfor;?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="uk-width-1-5"><a href="product.php">Buno Artikel</a></li>
                                <li class="uk-width-1-5"><a href="product.php">Beklebungen</a></li>
                                <li class="uk-width-1-5"><a href="product.php">Zeitungen</a></li>
                                <li class="uk-width-1-5"><a href="product.php">Tekstilen</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </section>
