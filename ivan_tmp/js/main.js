$(document).ready(function() {

    /*
    *  Scroll to top
    */
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 300) {
            $(".uk-totop").addClass("uk-active uk-animation-slide-bottom");
        } else {
            $(".uk-totop").removeClass("uk-active uk-animation-slide-bottom");
        }
    });

    // Footer Flip Slider
    var swiper = new Swiper('#footer .swiper-container', {
        effect: 'flip',
        nextButton: '#footer .swiper-button-next',
        prevButton: '#footer .swiper-button-prev',
    });

});
