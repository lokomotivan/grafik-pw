<?php include("_head.php");?>

<section class="tm-section">
    <div class="uk-container">
        <div id="main" class="uk-padding">

            <h2>Cart</h2>

            <?php

                print_r($_SESSION["cart"]);

            ?>

            <?php if(isset($_SESSION["cart"])):?>
                <?php $i = 0;?>
                <table class="uk-table uk-table-striped uk-table-middle">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($_SESSION["cart"] as $product):?>
                            <?php $c = $i++;?>
                            <tr class='cart-item-<?=$c?>'>
                                <td><?=$product["name"]?></td>
                                <td>$ <?=$product["price"]?></td>
                                <td class="uk-text-right"><button class="cart-remove" value="<?=$c?>">x</button></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>

                <div class="uk-text-right">
                    <a class="uk-button uk-button-primary" href="#">Checkoutt</a>
                </div>

            <?php else:?>
                <h5>Your cart is empty</h5>
            <?php endif;?>

        </div>
    </div>
</section>

<?php include("_foot.php");?>
