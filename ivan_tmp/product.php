<?php include("_head.php");?>

<section class="tm-section">
    <div class="uk-container">
        <div id="main" class="uk-padding">

            <h2>Plakate</h2>

            <table id="pricing" class="uk-table uk-table-small uk-table-middle uk-table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>10</th>
                        <th>50</th>
                        <th>100</th>
                        <th>1000</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($i = 0; $i < 5; $i++):?>
                        <tr>
                            <td>A<?=$i?></td>
                            <td>
                                <button value="10">$10</button>
                            </td>
                            <td>
                                <button value="50">$50</button>
                            </td>
                            <td>
                                <button value="100">$100</button>
                            </td>
                            <td>
                                <button value="1000">$1000</button>
                            </td>
                        </tr>
                    <?php endfor;?>
                </tbody>
            </table>

        </div>
    </div>
</section>

<?php include("_foot.php");?>
