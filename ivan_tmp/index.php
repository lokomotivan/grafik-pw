<?php include("_head.php");?>

<script>
    $(document).ready(function() {
        var swiper = new Swiper('#slider', {
            pagination: '#slider .swiper-pagination',
            paginationClickable: true,
            spaceBetween: 30,
            //autoplay: 5000,
        });
    });
</script>

<!-- SLIDER -->
<section class="tm-section">
    <div class="uk-container">
        <!-- Swiper -->
        <div id="slider"  class="swiper-container">
            <div class="swiper-wrapper">
                <?php for($i = 1; $i < 5; $i++):?>
                    <div class="swiper-slide uk-animation-fade">
                        <div class="slider-item uk-grid uk-grid-collapse" style="height:400px;">
                            <div class="uk-width-expand uk-flex uk-flex-middle">
                                <div class="uk-padding">
                                    <h3 class="uk-text-primary uk-margin-remove">Some Special Offer Subtitle <?=$i?></h3>
                                    <h2 class="uk-margin-remove">Special Offer Title <?=$i?></h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                            </div>
                            <div class="uk-width-1-3 uk-cover-container">
                                <img src="./images/navbar-img.jpg" uk-cover />
                            </div>
                        </div>
                    </div>
                <?php endfor;?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>

<!-- PANELS -->
<section class="tm-section">
    <div class="uk-container">
        <div class="panels-group">
            <div class="uk-child-width-1-3@m uk-grid-collapse" uk-grid>
                <div class="color-panel panel-primary">
                    <img class="uk-blend-darken" src="./images/references.jpg" uk-cover />
                    <a href="#">
                        <h3>Referenzen</h3>
                        <p class="uk-animation-slide-bottom">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </a>
                </div>
                <div class="color-panel panel-green">
                    <img src="./images/kontakt.jpg" uk-cover />
                    <a href="#">
                        <h3>Kontakt</h3>
                        <p class="uk-animation-slide-bottom">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </a>
                </div>
                <div class="color-panel panel-purple">
                    <img src="./images/versand_zahlung.jpg" uk-cover />
                    <a href="#">
                        <h3>Versand & Zahlung</h3>
                        <p class="uk-animation-slide-bottom">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- POPULAR PRODUCTS -->
<section class="tm-section">
    <div class="uk-container">
        <h2 class="uk-h3"><span class="uk-text-primary">Popular</span> Products</h2>
        <div id="popular-products" class="uk-child-width-1-4 uk-grid-small" uk-grid>
            <div>
                <div class="panel panel-hover">
                    <a class="uk-position-cover" href="#">
                        <img src="./images/product1.jpg" uk-cover />
                    </a>
                    <span class="uk-label">Flyers</span>
                </div>
            </div>
            <div>
                <div class="panel panel-hover">
                    <a class="uk-position-cover" href="#">
                        <img src="./images/product2.jpg" uk-cover />
                    </a>
                    <span class="uk-label">Kuverts</span>
                </div>
            </div>
            <div>
                <div class="panel panel-hover">
                    <a class="uk-position-cover" href="#">
                        <img src="./images/product3.jpg" uk-cover />
                    </a>
                    <span class="uk-label">T-Shirts</span>
                </div>
            </div>
            <div>
                <div class="panel panel-hover">
                    <a class="uk-position-cover" href="#">
                        <img src="./images/product4.jpg" uk-cover />
                    </a>
                    <span class="uk-label">Stickers</span>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("_foot.php");?>
