<?php
/**
 *  @author Ivan Milincic <lokomotivan@gmail.com>
 *
 */

// must start session
session_start();


/**
 *  Add To Cart
 *
 */
if(isset($_POST["product_id"])) {

    // create data array
    $data = array();

    /*
     *  if $_SESSION["cart"] exists
     *  asign it to $data array so we can keep existing $_SESSION["cart"] data
     *
     */
    if(isset($_SESSION["cart"])) {
        $data = $_SESSION["cart"];
    }

    // new product array
    $product = array(
        "id" => $_POST["product_id"],
        "name" => "Product-".rand(0,50),
        "price" => rand(0,1000),
    );

    // add new product to $data array
    array_push($data, $product);

    // set/update $_SESSION["cart"]
    $_SESSION["cart"] = $data;

    // return response to jquery so we can update cart
    $response = $product;
    $json = json_encode($response);
    echo $json;

}


/**
 *  Remove Item from cart
 *
 */
if(isset($_POST["remove_item"])) {

    $numb = $_POST["remove_item"];

    if(count($_SESSION["cart"]) <= 1) {
        unset($_SESSION["cart"]);
        $_SESSION["cart"] = array_values($_SESSION["cart"]);
    }else {
        unset($_SESSION["cart"][$numb]);
        $_SESSION["cart"] = array_values($_SESSION["cart"]);
    }

    // return response to jquery
    $response = count($_SESSION["cart"]);
    $json = json_encode($response);
    echo $json;

}


?>
