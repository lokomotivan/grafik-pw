
<footer id="footer" class="uk-padding">
    <div class="uk-container">
        <div class="uk-grid-divider uk-child-width-1-3@m" uk-grid>

            <div>
                <div id="flip-slider">
                    <!-- Swiper -->
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide" style="background-image:url(./images/flip-slide.jpg);"></div>
                            <div class="swiper-slide" style="background-image:url(./images/product1.jpg);"></div>
                            <div class="swiper-slide" style="background-image:url(./images/product2.jpg);"></div>
                            <div class="swiper-slide" style="background-image:url(./images/product3.jpg);"></div>
                            <div class="swiper-slide" style="background-image:url(./images/product4.jpg);"></div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-prev">
                            <?php echo file_get_contents("images/left.svg");?>
                        </div>
                        <div class="swiper-button-next">
                            <?php echo file_get_contents("images/right.svg");?>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <h3>Kontakt</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <ul class="contact-info uk-list uk-margin-small">
                    <li>
                        <span class="svg-icon"><?php echo file_get_contents("images/world.svg");?></span> Beispieladresse 24, Austria
                    </li>
                    <li>
                        <span class="svg-icon"><?php echo file_get_contents("images/envelope.svg");?></span> beispielemail@email.com
                    </li>
                    <li>
                        <span class="svg-icon"><?php echo file_get_contents("images/talking.svg");?></span> +321 456 9876
                    </li>
                </ul>
                <h4 class="uk-margin-remove">Folge Uns Auf</h4>
                <div class="social uk-margin-small">
                    <a href="#">
                        <span class="svg-icon"><?php echo file_get_contents("images/facebook.svg");?></span>
                    </a>
                    <a href="#">
                        <span class="svg-icon"><?php echo file_get_contents("images/instagram.svg");?></span>
                    </a>
                </div>
            </div>

            <div>
                <h3>versand & zahlung</h3>
                <img src="./images/paypal.png" />
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </div>

        </div>
        <hr class="uk-margin-medium-top" />
        <div class="uk-text-center uk-text-small uk-text-muted">
            Copyright © 2017 PWthemes. All rights reserved.
        </div>
    </div>

    <!-- scroll to top -->
    <a class="uk-visible@l" href="#" uk-totop uk-scroll></a>

</footer>

<!-- CART -->
<div id="cart" uk-offcanvas="flip: true; overlay: true">
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" uk-close></button>

        <h3>Cart</h3>

        <table id="cart-table" class="uk-table uk-light uk-table-small uk-table-middle">
            <tbody>
                <?php if(isset($_SESSION["cart"])):?>
                    <?php $i = 0;?>
                    <?php foreach($_SESSION["cart"] as $product):?>
                        <?php $c = $i++;?>
                        <tr class='cart-item-<?=$c?>'>
                            <td><?=$product["name"]?></td>
                            <td>$<?=$product["price"]?></td>
                            <td><button class="cart-remove" value="<?=$c?>">x</button></td>
                        </tr>
                    <?php endforeach;?>
                <?php endif;?>
            </tbody>
        </table>

        <a class="uk-button uk-button-primary uk-width-1-1" href="cart.php">Go To Cart</a>

    </div>
</div>

</body>
</html>
