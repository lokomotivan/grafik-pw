<div id="main">
  <?= $page->render('title') ?>
  <?= $page->render('body') ?>
  <?= wireRenderFile('fields/_children', ['value' => $page->children]) ?>
</div>