<div id="main">
  <?= $page->render('title') ?>
  <?= $page->render('coverpic') ?>
  <?= $page->render('pricing') ?>
  <?= $page->render('gallery') ?>
  <?= wireRenderFile('fields/_siblings') ?>
</div>

<region id="mainsection">

        <section class="tm-section">
            <div class="uk-container">
                <?= $page->render('title') ?>
                <div class="content">
                    <div  class="uk-grid uk-grid-divider uk-grid-collapse" uk-grid>
                        <div class="uk-width-2-5@m">
                            <div class="product-img uk-padding uk-text-center">
                                <?= $page->render('coverpic') ?>
                            </div>
                        </div>
                        <div class="uk-width-expand@m">
                            <div class="product-desc uk-padding">
                                <?= $page->render('body') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php if($page->gallery->count):?>
            <section class="product-gallery tm-section">
                <div class="uk-container">
                    <h3>Galerie</h3>
                    <div class="content">
                        <?= $page->render('gallery') ?>
                    </div>
                </div>
            </section>
        <?php endif;?>

        <section class="tm-section">
            <div class="uk-container">
                <?= $page->render('pricing') ?>
            </div>
        </section>

</region>
