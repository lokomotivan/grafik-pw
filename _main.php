<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>grafikgesellen.at - <?= $page->title ?></title>
  <meta name="description" content="<?= $page->googledesc ?: pages(1)->googledesc ?>" />

  <link rel="shortcut icon" href="docs/images/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

  <!-- css -->
  <link rel="stylesheet" href="<?= $config->urls->templates ?>css/swiper.min.css">
  <link rel="stylesheet" href="<?= AIOM::css('less/theme.less') ?>">

  <!-- scripts -->
  <!--<script src="//code.jquery.com/jquery-2.2.0.min.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="<?= $config->urls->templates ?>js/uikit.min.js"></script>
  <script src="<?= $config->urls->templates ?>js/uikit-icons.min.js"></script>
  <script src="<?= $config->urls->templates ?>js/cart.js"></script>
  <script src="<?= $config->urls->templates ?>js/swiper.min.js"></script>
  <script src="<?= $config->urls->templates ?>js/js.cookie.js"></script>

  <link rel="stylesheet" href="<?= $config->urls->templates ?>css/custom.css">
  <script src="<?= $config->urls->templates ?>js/main.js"></script>

</head>

<body>

  <!-- HEADER -->
  <region id="header"></region>
  <region id="breadcrumbs"></region>

  <!-- SLIDER -->
  <region id="slidersection"></region>

  <!-- PANELS -->
  <region id="panelssection"></region>

  <!-- POPULAR PRODUCTS -->
  <region id="popularsection"></region>

  <!-- MAIN CONTENT -->
  <region id="mainsection">
    <section class="tm-section">
      <div class="uk-container">
        <div id="main" class="uk-padding"></div>
      </div>
    </section>
  </region>

  <!-- FOOTER -->
  <region id="footer"></region>

  <!-- Mobile Menu -->
  <div id="mobile-menu" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>

        <ul class="uk-nav uk-nav-primary uk-margin-top uk-nav-parent-icon" uk-nav>
            <?php
            $menuitems = pages('template=products,parent=1');
            $num = $menuitems->count();
            foreach($menuitems as $item): ?>
            <?php
                // active class
                $active = ($page->id == $item->id || $page->rootParent->id == $item->id) ? "uk-active uk-open" : "";
            ?>

                <?php if($item->children()->count):?>
                    <li class="uk-parent <?= $active ?>">
                        <a href="<?= $item->url ?>"><?= $item->title ?></a>
                        <ul class="uk-nav-sub">
                            <?php foreach($item->children as $child): ?>
                                <?php
                                    $child_active = ($page->id == $child->id) ? "uk-active" : "";
                                ?>
                                <li class="<?= $child_active ?>">
                                    <a href="<?= $child->url ?>"><?= $child->title ?></a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                <?php else:?>
                    <li class="<?= $active ?>">
                        <a href="<?= $item->url ?>"><?= $item->title ?></a>
                    </li>
                <?php endif;?>

            <?php endforeach;?>
        </ul>

    </div>
  </div>

  <region id="footerscripts"></region>
</body>
</html>
