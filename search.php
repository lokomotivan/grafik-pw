<?php namespace ProcessWire;
$query = sanitizer()->text(input('get')->query);
$val = sanitizer()->selectorValue($query);
$sel = 
  "template=product|products|portfolioitem".
  ",title|body%='$val'".
  ",limit=100"
  ;
$results = $val ? pages($sel) : new PageArray();
?>


<div id="main">
  <h2>Suche nach "<?= $query ?>"</h2>

  <?php foreach($results as $result): ?>
    <div class="uk-padding">
      <p><strong><a href="<?= $result->url ?>"><?= $result->title ?></a></strong></p>
      <?= truncateText($result->body) ?>
    </div>
  <?php endforeach; ?>

  <?php if(!$results->count()) echo '<p><strong>Leider wurden keine Ergebnisse gefunden!</strong></p>'; ?>
  
  <p>Vielleicht hilft Ihnen auch unsere <a href="<?= pages()->get('template=sitemap')->url ?>">Sitemap</a> weiter...</p>
</div>