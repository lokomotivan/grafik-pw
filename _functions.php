<?php namespace ProcessWire;

function truncateText($text, $maxlength = 200) {
  // truncate to max length
  $text = substr(strip_tags($text), 0, $maxlength);
  // check if we've truncated to a spot that needs further truncation
  if(strlen(rtrim($text, ' .!?,;')) == $maxlength) {
    // truncate to last word 
    $text = substr($text, 0, strrpos($text, ' ')); 
  }
  return trim($text); 
}

function svg($name) {
  $dir = getcwd();
  chdir(config('paths')->templates);
  $svg = file_get_contents("images/$name.svg");
  chdir($dir); // go back to actual path to not cause any troubles in other scripts

  // replace doctype that causes problems with the markup regions renderer
  // https://github.com/processwire/processwire-issues/issues/336
  if(strpos($svg, '<!DOCTYPE svg') !== false) {
    $search = '/<!DOCTYPE svg(.*?)>/';
    $svg = preg_replace($search, '', $svg);
  }

  return "<span class='svg-icon'>$svg</span>";
}