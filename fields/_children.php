<?php namespace ProcessWire;
if(!$page->children->count()) return;
?>

<div class="uk-child-width-1-4@s uk-text-center" uk-grid>
  <?php foreach($page->children as $item): ?>
    <div>
        <div class="panel panel-hover uk-flex uk-flex-middle uk-flex-center">
            <div>
                <?php if($item->coverpic) echo "<img src='{$item->coverpic->maxSize(200,200)->url}'>"; ?>
                <span class="uk-label"><?= $item->title ?></span>
                <a class="uk-position-cover" href="<?= $item->url ?>"></a>
            </div>
        </div>
    </div>
  <?php endforeach; ?>
</div>
