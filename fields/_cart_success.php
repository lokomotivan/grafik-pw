<?php namespace ProcessWire;
$post = input('post');

// spamfilter
if($post->password) session()->redirect('/cart/');

// log this request
$p = new Page();
$p->template = 'order';
$p->name = uniqid();
$p->title = date('d.m.Y') . " - $sum € - {$post->name} ({$post->mail})";
$p->parent = pages()->get('template=orders');

$p->body =
  "<p>Name: {$post->name}<br>".
  "E-Mail: {$post->mail}<br>".
  "Telefon: {$post->tel}<br>".
  "Kommentar:</p><blockquote>" . nl2br($post->comment) . "</blockquote>".
  "<p></p>";

$i = 0;
foreach(config('cart') as $item) {
  $p->body .= '<p>';
  $p->body .= '<strong><a href="' . pages($item['pid'])->httpUrl . '">' . pages($item['pid'])->title . ' (#' . $item['pid'] . ')</a></strong><br>';
  $p->body .= '<strong>Anzahl:</strong> ' . $item['amount'] . '<br>';
  $p->body .= '<strong>Variante:</strong> ' . $item['size'] . '<br>';
  $p->body .= '<strong>Preis:</strong> ' . $item['price'] . '<br>';
  $p->body .= '<strong>Kommentar:</strong><br><blockquote>' . $post->itemcomments[$i] . '</blockquote>';
  $p->body .= '</p>';

  $i++;
}

$p->body .= "<h3>Summe: $sum</h3>";

$p->save();

// send email
$mail = wireMail();
$mail->to('office@grafikgesellen.at')->from('office@grafikgesellen.at'); // all calls can be chained
$mail->subject('Neue Anfrage auf grafikgesellen.at');
$mail->bodyHTML("<html><body>{$p->body}</body></html>"); 
$mail->send();

// clear cart
setcookie('cart', null, strtotime('+30 days'), '/');
config()->cart = null;
echo wireRenderFile('fields/_header'); // redraw the header to apply changes
?>

<div id="main">
  <?php
  $page->title = 'Danke für Ihre Anfrage!';
  echo $page->render('title');
  ?>

  <?= $page->cartsuccess ?>
</div>

