<?php
if(!$value OR !$value->count()) return;
?>

<!-- SLIDER -->
<section pw-append="slidersection" class="tm-section">
  <div class="uk-container">
    <!-- Swiper -->
    <div id="slider"  class="swiper-container">
      <div class="swiper-wrapper">
        <?php foreach($value as $slide): ?>
          <div class="swiper-slide uk-animation-fade">
            <div class="slider-item uk-grid uk-grid-collapse uk-overflow-hidden" style="height:400px;">
              <div class="uk-width-expand@m uk-flex uk-flex-middle">
                <div class="uk-padding uk-width-1-1">
                  <h2 class="uk-margin-remove"><?= $slide->title ?></h2>
                  <?php if($slide->subtitle) echo "<h3 class='uk-text-primary uk-margin-remove'>{$slide->subtitle}</h3>"; ?>

                  <div class="uk-visible@m">
                      <?= $slide->body ?>
                  </div>

                  <?php if($slide->button_href): ?>
                    <a href="<?= $slide->button_href->url ?>" class="uk-button uk-button-primary"><?= $slide->button_text ?: 'Details' ?></a>
                  <?php endif; ?>
                </div>
              </div>
              <div class="uk-width-1-2@m uk-flex uk-flex-middle uk-flex-center">
                <div class="uk-padding-small">
                    <img src="<?= $slide->sliderpic->height(400)->url ?>" />
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
    </div>
  </div>
</section>
<script pw-append="footerscripts">
  $(document).ready(function() {
    var swiper = new Swiper('#slider', {
      pagination: '#slider .swiper-pagination',
      paginationClickable: true,
      spaceBetween: 30,
      //autoplay: 5000,
    });
  });
</script>
