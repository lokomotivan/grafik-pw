<?php namespace ProcessWire;
$pop = pages(1)->popular;
$num = $pop->count();
if($num): ?>
  <section pw-append="popularsection" class="tm-section">
    <div class="uk-container">
      <h2 class="uk-h3"><span class="uk-text-primary">Popular</span> Products</h2>
      <div id="popular-products" class="uk-child-width-1-<?=$num?>@l uk-child-width-1-2@s uk-grid-small" uk-grid>
        <?php foreach($pop as $item): ?>
          <?php
          if(!$item->coverpic) $item->coverpic = pages(1)->coverpic;
          ?>
          <div>
            <div class="panel panel-hover">
              <a class="uk-position-cover" href="<?= $item->url ?>">
                <img src="<?= $item->coverpic->height(300)->url ?>" uk-cover />
              </a>
              <span class="uk-label"><?= $item->title ?></span>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
