<?php
if(!$value) return;

if($page->editable) $editicon = "<a href='{$page->editUrl}' class='uk-margin-left'><span uk-icon='icon: file-edit'></span></a>";
else $editicon = '';
?>

<h1>
  <?= $value.$editicon ?>
</h1>
