<?php namespace ProcessWire; ?>

<h3>Portfolio</h3>

<div id="flip-slider">
  <!-- Swiper -->
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php foreach(pages('template=portfolioitem,coverpic.count>0,sort=random,limit=10') as $item): ?>
        <div class="swiper-slide" style="background-image:url(<?= $item->coverpic->height(350)->url ?>);"></div>
      <?php endforeach; ?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-prev">
      <?= svg("left"); ?>
    </div>
    <div class="swiper-button-next">
      <?= svg("right"); ?>
    </div>
  </div>
</div>

<div class="uk-text-center uk-margin-small-top"><a href="<?= pages()->get('template=portfolio')->url ?>">Alle Referenzen anzeigen</a></div>
