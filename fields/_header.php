<?php namespace ProcessWire;
chdir(config('paths')->templates);

// here we use the region explicitly so that we are able to redraw the header
// at a later time (replace/overwrite it). that would not be possible
// using pw-append on the section element. see _cart_success.php
?>
<region id="header">
  <section>
    <div class="uk-container">

      <div id="headerbar" class="uk-flex uk-flex-middle uk-flex-between">
        <div class="logo">
          <a href="<?= $pages->get("/")->url ?>">
            <img src="<?= pages(1)->logo->height(60)->url ?>" alt="Logo <?= pages(1)->title ?>" />
          </a>
        </div>
        <div class="uk-visible@m">
          <form action="<?= $pages->get("/suche/")->url ?>" method="get" class="uk-form">
            <div class="uk-inline">
              <span class="uk-form-icon" uk-icon="icon: search"></span>
              <input class="uk-input" name="query">
            </div>
          </form>
        </div>
        <div class="uk-flex">
          <div class="social uk-visible@s">
            <a href="<?= pages()->get('template=contact')->facebook ?>" target="_blank">
              <span class="svg-icon"><?= svg("facebook"); ?></span>
            </a>
            <a href="<?= pages()->get('template=contact')->instagram ?>" target="_blank">
              <span class="svg-icon"><?= svg("instagram"); ?></span>
            </a>
          </div>

          <a id="top-cart" href="<?= $pages->get("/cart/")->url ?>">
            <span class="svg-icon"><?= svg("shopping-basket"); ?></span>
            <span id="top-cart-count" class="uk-badge"><?= count($config('cart')) ?></span>
          </a>
        </div>
      </div>

      <div id="navbar" class="uk-visible@m">
        <nav class="uk-navbar-container" uk-navbar>
          <div class="uk-navbar-center uk-width-1-1">
            <ul class="uk-navbar-nav uk-width-1-1">
              <?php
              $menuitems = pages('template=products,parent=1');
              $num = $menuitems->count();
              foreach($menuitems as $item): ?>
              <?php
                    /**
                     *  dropdown columns up to 6 item 1 column,
                     *  12 items 2 cllumns,
                     *  more then 12 full width
                     *
                     */
                    $children_numb = $item->children()->count;
                    if($children_numb <= 6) {
                        $dropdown_cls = "tm-dropdown-single";
                        $dropdown_column = "";
                        $media_width = "1-2";
                    }elseif($children_numb <= 12){
                        $dropdown_cls = "tm-dropdown-double";
                        $dropdown_column = " uk-column-1-2";
                        $media_width = "auto";
                    }else {
                        $dropdown_cls = "tm-dropdown-full";
                        $dropdown_column = " uk-column-1-3";
                        $media_width = "auto";
                    }
                    // active class
                    $active = ($page->id == $item->id || $page->rootParent->id == $item->id) ? "uk-active" : "";
              ?>
                <li class="uk-width-1-<?=$num?> <?= $active ?> uk-parent">
                  <a href="<?= $item->url ?>"><span><?= $item->title ?></span></a>
                 <div class="uk-navbar-dropdown <?= $dropdown_cls ?>">
                    <div class="uk-navbar-dropdown-grid uk-grid" style="margin:0;">
                          <?php if($item->coverpic): ?>
                            <div class="dropdown-img uk-width-<?= $media_width ?> uk-flex uk-flex-middle uk-flex-center" style="padding-left:0;max-width:300px;">
                              <div>
                                <img src="<?= $item->coverpic->height(300)->url ?>" />
                              </div>
                            </div>
                          <?php endif; ?>
                          <div class="dropdown-items uk-width-expand">
                            <ul class="uk-nav uk-navbar-dropdown-nav<?= $dropdown_column?>">
                              <?php foreach($item->children as $child): ?>
                                <li>
                                    <a class="uk-text-uppercase" href="<?= $child->url ?>"><?= $child->title ?></a>
                                </li>
                              <?php endforeach; ?>
                            </ul>
                          </div>
                    </div>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </nav>
      </div>

        <div id="mobile-menu-button" class="uk-hidden@m">
            <a href="#mobile-menu" uk-toggle>
                <span uk-icon="icon: menu"></span>
            </a>
        </div>

    </div>
  </section>
</region>
