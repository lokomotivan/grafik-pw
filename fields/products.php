<?php namespace ProcessWire;
if(!$value OR !$value->count()) return;
?>

<h3>Gleich bestellen:</h3>

<div class="uk-child-width-expand@s uk-text-center" uk-grid>
  <?php foreach($value as $item): ?>
    <div>
      <div class="uk-card uk-card-default uk-card-body">
        <a href="<?= $item->url ?>"><?= $item->title ?>
        <?php if($item->coverpic) echo "<br><img src='{$item->coverpic->maxSize(200,200)->url}'>"; ?>
        </a>
      </div>
    </div>
  <?php endforeach; ?>
</div>
