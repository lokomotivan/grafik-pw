<?php namespace ProcessWire;
chdir(config('paths')->templates);
?>

<footer id="footer" pw-append="footer" class="uk-padding">
  <div class="uk-container">
    <div class="uk-grid-divider uk-child-width-1-3@m" uk-grid>

      <div>
        <?= wireRenderFile('fields/_portfolioslider') ?>
      </div>

      <div>
        <?php $contact = pages()->get('template=contact'); ?>
        <h3><?= $contact->title ?></h3>
        <?= $contact->shortbody ?>
        <?= wireRenderFile('fields/_contact') ?>
      </div>

      <div>
        <h3>versand & zahlung</h3>
        <?= pages()->get('template=paymentinfo')->shortbody; ?>
        <div class="icon-large uk-text-center"><?= file_get_contents("images/Lets-Encrypt-Seal-SSL.svg"); ?></div>
      </div>

    </div>
    <hr class="uk-margin-medium-top" />
    <div class="uk-text-center uk-text-small uk-text-muted">
      Copyright © 2017 PWthemes. All rights reserved. |
      <a href="<?= pages()->get('template=sitemap')->url ?>"><?= pages()->get('template=sitemap')->title ?></a> | 
      <a href="<?= pages()->get('template=imprint')->url ?>"><?= pages()->get('template=imprint')->title ?></a>
      <br>
      <small>website by <a href="https://www.baumrock.com" title="baumrock.com: Websites und web-basierte Individualsoftware" target="_blank">baumrock.com</a></small>
    </div>
  </div>

  <!-- scroll to top -->
  <a class="uk-visible@l" href="#" uk-totop uk-scroll></a>

</footer>