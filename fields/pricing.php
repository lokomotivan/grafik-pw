<?php
if(!$value->filledInTable()) return;

$rows = $value->getRows();
array_shift($rows); // remove header
array_pop($rows); // remove last row (always empty)
?>
<h3>Preise (Richtwerte):</h3>
<div class="pricing-container">
  <table id="pricing" class="uk-table uk-table-small uk-table-middle uk-table-striped uk-text-center">
    <thead>
      <tr>
        <?php foreach($value->getRow(0) as $i => $cell) {
          if($cell === null AND $i>0) continue;
          echo "<th class='uk-text-center'>{$cell}</th>";
        } ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach($rows as $r=>$row): ?>
        <tr>
          <?php foreach($row as $c=>$cell) {
            if($cell == '') continue;
            echo "<td>";
            if($c > 0) echo "<a href='{$config->urls->root}cart/?pid=$page&row=".($r+1)."&col=$c' class='uk-text-nowrap'>€ ";
            echo "<span>$cell</span>";
            if($c > 0) echo "</a>";
            echo "</td>";
          } ?>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
