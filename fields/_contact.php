<?php namespace ProcessWire;
chdir(config('paths')->templates);
$contact = pages()->get('template=contact');
?>

<ul class="contact-info uk-list uk-margin-small">
  <li>
    <span class="svg-icon"><?= svg('world') ?></span>
    <a href="https://www.google.at/maps/search/<?= $contact->address ?>" target="_blank"><?= $contact->address ?></a>
  </li>
  <li>
    <span class="svg-icon"><?= svg('envelope') ?></span>
    <a href="mailto:<?= $contact->email ?>"><?= $contact->email ?></a>
  </li>
  <li>
    <span class="svg-icon"><?= svg('talking') ?></span>
    <a href="tel:<?= $contact->tel ?>"><?= $contact->tel ?></a>
  </li>
</ul>
<h4 class="uk-margin-remove">Folge Uns Auf</h4>
<div class="social uk-margin-small">
  <a href="<?= $contact->facebook ?>">
    <span class="svg-icon"><?= svg('facebook') ?></span>
  </a>
  <a href="<?= $contact->instagram ?>">
    <span class="svg-icon"><?= svg('instagram') ?></span>
  </a>
</div>
