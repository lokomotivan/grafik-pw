<?php namespace ProcessWire;
$siblings = $page->siblings("id!=$page");
if(!$siblings OR !$siblings->count()) return;
?>

<hr><p>Andere Produkte in der Kategorie <?= $page->parent->title ?>:

<?php
$del = '';
foreach($siblings as $item) {
  echo "$del<a href='{$item->url}'>{$item->title}</a>";
  $del = ' | ';
}
?>
