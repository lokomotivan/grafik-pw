<?php
if(!$value OR !$value->count()) return;
?>

<!--<h3>Galerie</h3>-->
<div class="uk-child-width-expand@m uk-child-width-1-2@s uk-grid-small uk-grid-collapse" uk-grid uk-lightbox="animation: scale">
  <?php foreach($value as $i=>$pic): ?>
    <div class="uk-flex uk-flex-middle uk-flex-center">
      <a class="uk-padding-small uk-display-block uk-text-center uk-width-1-1" href="<?= $pic->maxSize(800, 800)->url ?>" caption="<?= $pic->description ?>">
        <img src="<?= $pic->height(130)->url ?>" alt="grafikgesellen.at Galerie <?= $page->title ?> Produktbild <?= $i+1 ?>">
      </a>
    </div>
  <?php endforeach; ?>
</div>
