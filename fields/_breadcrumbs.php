<?php namespace ProcessWire;
if($page->parents->count < 2) return;
?>

<section pw-append="breadcrumbs" class="tm-section uk-visible@m">
  <div class="uk-container">
    <div id="breadcrumbs" class="uk-display-inline-block">
        <a href="<?= $pages->get("/")->url ?>"><span uk-icon="icon: home"></span></a>
        <?php foreach($page->parents->remove(pages(1)) as $p) echo "<a href='{$p->url}'>{$p->title}</a>"; ?>
        <span><?= $page->title ?></span>
    </div>
  </div>
</section>
