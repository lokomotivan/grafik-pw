<?php
if(!$value OR !$value->count()) return;
?>

<!-- PANELS -->
<section pw-append="panelssection" class="tm-section">
  <div class="uk-container">
    <div class="panels-group">
      <div class="uk-child-width-1-3@s uk-grid-collapse" uk-grid>
        <?php foreach($value as $i=>$item): ?>
          <?php
          $col = 'panel-primary';
          if(($i+1)%2==0) $col = 'panel-green';
          if(($i+1)%3==0) $col = 'panel-purple';
          ?>
          <div class="color-panel <?=$col?>">
            <img class="uk-blend-darken" src="<?= $item->panelpic->maxSize(300,300)->url ?>" uk-cover />
            <a href="<?= $item->pageref->url ?>">
              <h3><?= $item->pageref->title ?></h3>
              <p class="uk-animation-slide-bottom">
                <?= $item->shorttext ?>
              </p>
            </a>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>
