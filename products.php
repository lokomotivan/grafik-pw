

<!-- hide content of mainsection -->
<region id="mainsection">
    <div id="grid" class="tm-section">
        <div class="uk-container">
            <?= $page->render('title') ?>
            <p>Unsere Produkte in der Kategorie <?= $page->title ?>:</p>
            <?= wireRenderFile('fields/_children') ?>
        </div>
    </div>
</region>
