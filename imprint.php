<div id="main">
  <?= $page->render('title') ?>
  <?= $page->body ?>

  <div class="uk-text-small">
    Technische Umsetzung: <a href="https://www.baumrock.com" title="baumrock.com: Websites und web-basierte Individualsoftware" target="_blank">baumrock.com</a>
  </div>
</div>