$(document).ready(function() {

    /**
     *  Update Cart
     *
     */
    $(document).on('click', '#pricing button', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: { product_id: "123" },
            dataType: "json",
            success: function(data) {
                console.log(data);

                // update cart table
                var table = "<tr class='cart-item-"+cartCount+"'>";
                table += "<td>"+ data.name +"</td>";
                table += "<td>$"+ data.price +"</td>";
                table += "<td><button class='cart-remove' value='"+cartCount+"'>x</button></td>";
                table += "</tr>";
                $('#cart-table tbody').append(table);

            },
            error: function(data) {
                console.log("Ajax Error");
            }
        });

        // product value
        var price = $(this).val();

        // get cart value (numb of products)
        var cartCount = $("#top-cart").val();
        // add 1
        var newCartCount = parseInt(cartCount) + 1;
        // update top cart count
        $("#top-cart").val(newCartCount);
        $("#top-cart-count").text(newCartCount);


    });

    /**
     *  Remove from cart
     *
     */
     $(document).on('click', '.cart-remove', function(e) {
        e.preventDefault();

        var removeID = $(this).val();

        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: { remove_item: removeID },
            dataType: "json",
            success: function(data) {
                console.log(data);
            },
            error: function(data) {
                console.log("Ajax Error");
            }
        });

        $(".cart-item-"+removeID).empty();

        // get cart value (numb of products)
        var cartCount = $("#top-cart").val();
        // take 1
        var newCartCount = parseInt(cartCount) - 1;
        // update top cart count
        $("#top-cart").val(newCartCount);
        $("#top-cart-count").text(newCartCount);


    });

});
