<div id="main">
	<?= $page->render('title') ?>
	<?= $page->render('coverpic') ?>
	<?= $page->render('body') ?>
	<?= $page->render('gallery') ?>
	<?= $page->render('products') ?>
</div>